#!/bin/bash

if [ ! -e /bootstrap.lock ]; then
    echo "*** You need firebase CLI setup. ***"
    echo ""
    echo "$ docker exec -it [firebase container] bash"
    echo "# firebase login"
    echo "# firebase init"
    echo "# exit"
    echo "$ docker restart [firebase container]"

    touch /bootstrap.lock
    tail -f /dev/null
fi

firebase serve -p 5000 -o 0.0.0.0

