serve:
	./node_modules/.bin/firebase serve -p 5000

container:
	docker-compose up -d
	docker logs firebase-test
	docker exec -it firebase-test bash
	docker restart firebase-test

container-rebuild:
	docker-compose down
	docker rmi firebasetest_firebase-test
	docker-compose up -d
	docker logs firebase-test
	docker exec -it firebase-test bash
	docker restart firebase-test
